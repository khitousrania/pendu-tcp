# Projet programmation réseau : jeu du pendu (server-clients).

## Présentation du jeu et de sa logique 

Ce projet représente le jeu du pendu. Il est développé en utilisant les sockets en programmation C, précisément le protocole de la couche transport TCP.

Le server ne permet pas aux nouveaux clients de se connecter lorsque y a 2 clients qui sont déjà connectées et prend soin d'informer ces clients refusées de la non-autorisation de connection.

Ce jeu peu se jouer en paire de clients càd 2 clients également seule.

Le premier client qui termine est considéré comme jouant seule car il n'existe pas un autre client qui a terminé avant lui et donc un score et un état (Gagnant ou Perdant) qui lui seront attribué et affiché avant sa déconnection qui représente la fin de la partit pour lui et cela afin de le pas obliger d'attendre le deuxième client.

Le deuxième client qui termine cependant aura une proposition de comparer ou pas son score avec le premier client qui a terminé et auquel j'ai affiché le score et cela en lui précisant si le 1er client avait répondu sur le même type de questions (JAVA ou C) que lui.

NB : 
Il est possible de jouer seule càd n'être comparé à aucun autre client. Le premier qui termine est automatiquement non comparé mais le 2éme qui termine aura le choix de se comparer au premier client comme déja évoquée.

## Etapes lancement et compilation 

1-Ouvrir 3 ou 4 fenêtres du terminal linux. Une pour le server et deux fenêtres pour les clients si vous voulez jouer une partie paire sinon une seule fenêtre. 

2-Lancez la commande make qui permettra de compiler le projet pour créer les executables server et client.

3-Pour lancer le serveur entrer la commande ./server dans le terminal.

4-Pour lancer le client entrer ./client dans le terminal ou les terminaux selon le scénario que vous voudrez mettre en place.

## Teste du projet : 

Quand est-ce qu'un client est considéré comme joueur seule : 
*Quand il est le premier à terminer la partie entre les deux clients qui qui ont commencé le jeu.

Après avoir lancé le serveur.
#### Test 1-1:

Connecter un seule et premier client au serveur.

Le client connecté sera prié de choisir le type de QCM dont il voudra répondre (si JAVA il tape 1 sinon s'il voudra un QCM du language C il tape 2).

Le client commence à rentrer les réponses une après l'autre (les réponses rentrées par le client doivent correspondre au nombre digit de l'une des réponses proposées).

A la fin du jeu:
S'il était le seul connecté et le premier à soumettre dans la paire de clients ou bien le premier client à avoir terminé dans la paire de clients (2 clients connectés) il n'aura pas la question de choix s'il voulait comparer son score mais il aurait directement son score.

#### Test 1-2:
Connecter deux clients au server 

Le client connecté sera prié à choisir le type de QCM dont il voudra répondre (si JAVA il tape 1 sinon s'il voudra un QCM du language C il tape 2).

Les clients commencent à rentrer les réponses une après l'autre (les réponses rentrées par le client doivent correspondre au nombre digit de l'une des réponses proposées).

Le deuxième qui termine dans la paire de clients connectés, une question lui sera attribuer afin de lui demander s'il voudra comparer son score ou pas avec le premier client qui a terminé et bien sûr en lui disant si le premier client qui a terminé avait choisi le même QCM que lui. 

Si vous choisissez de ne pas être comparé alors il sera considéré comme en étant le seul à jouer comme le premier qui a terminé le jeu.

NB: Si un deuxiéme client se connecte aprés la fin de partie du premier client dans la paire il sera traité en étant 2éme client et il pourra comparer son score avec le premier client qui a terminé. 

#### Test 2:
 Connection de 3 clients à la fois. Le troisième sera rejeté car c'est un jeu qui se joue maximum à 2.

