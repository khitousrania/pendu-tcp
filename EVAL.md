# EVAL FILE
## Les bugs: 
Pour cette version, j'ai essayé de capter tous les bugs en testant plusieurs scénarios et de les résoudre. 

Un des bugs est d'avoir erreur "fread failer" côté serveur lorsque le deuxième client qui soumettra ses réponses repend à la question du choix de comparaison ou pas du score. Mais j'ai fini par trouver un code en commun pour le fichier "client.c" entre le premier client qui termine et le deuxième. Oui ça apparait simple mais le nombre de freads et de fwrite change pour ses deux cas.

Un des autres bugs est de gérer le calcule de score où un des clients d'une seule paire(2clients) se déconnecte et qu'un nouveau client se connecte, mais j'ai fini par trouver une solution en sorte qu'à chaque fin de partie pour 1 paire de clients, la nouvelle paire de clients pourra se connecter sans déconnecter le server.
  
## Temps pour le développement : 

Je n'avais pas bien avancé au début à cause de la charge du temps. Mais j'ai finalement pu me concentrer et trouver une bonne organisation.
 
## Evolutions

Je reconnais que j'ai eu plusieurs idées pour améliorer mon projet mais que je n'ai malheureusement pas eu suffisamment de temps pour les mettre en place.

1- Mettre en place une fonctionnalité qui permettra de vérifier les entrées du client si elles correspondent à un des numéro digit des réponses proposées et que si le client rentre n'importe quoi je lui repose la question. Pour l'instant mon programme prend toutes les entrées mais ne repose pas la question en cas d'erreur de la part du client. Temps : une demi-journée et cela afin d'adapter cette fonctionnalité à chacune des questions posées et faire les tests.

2- Ajout de l'option d'affichages des indices des réponses par ordre alphabétique et adapter également la première évolution proposée à celle-ci. Temps : une demi-journée et cela afin de bien tester les conséquences de cet ajout sur le projet (vérifications de réponses pour affichages de score).
