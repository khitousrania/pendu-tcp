#ifndef STRUCTURES_H
#define STRUCTURES_H

#include "constantes.h"

struct clientStructure{
    int8_t idClient;
    char idLangageChosen;  
    int8_t numberNextQuestion; 
    char responseBuffer[BUFFER_RESPONSES_LENGTH];
    char qcm[LENGTH_PACKET];
    int8_t numberOfQuestions;
    int8_t finalScore;
    int8_t numberOfOtherClient;
    char state;
};

struct clientEndScores{
    int idClient;
    int8_t endScore;
    char state;
    char idLangageChosen;
};



#endif