#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "errhandlers.h"
#include "functions.h"
#include "structures.h"
#include "constantes.h"

int main(){
    int server_sock,nb_fdopens;
    struct sockaddr_in server, client;
    fd_set active_fd_set, read_fd_set;
    if (connectionServer(&server_sock, &server, &active_fd_set))
        return EXIT_FAILURE;

    const int NB_MAX_CLIENT = 2; 
    FILE *fd_clients[NB_MAX_CLIENT];
    size_t nb_fread,nb_fwrite;

    socklen_t size_sockaddr = sizeof(struct sockaddr_in);
    int NB_MAX_CLIENT_UTILS = 0;
    
    int8_t lengthStructClientEndScores = 0;
    char buffer[BUFSIZ];
    memset(buffer, '\0', BUFSIZ);
    struct clientEndScores clientEndScores[NB_MAX_CLIENT];
    struct clientStructure clientStructureSent;

    while (true){

        read_fd_set = active_fd_set;
        if ((nb_fdopens = select(NB_MAX_CLIENT+4, &read_fd_set, NULL, NULL, NULL)) == -1){
            syserr("server : select failed");
            return EXIT_FAILURE;
        }

        for (int idClient = 0; idClient <  NB_MAX_CLIENT+4; idClient++){
            if (FD_ISSET(idClient, &read_fd_set)){
                if(idClient == server_sock){
                    int fd_socket_just_connected;
                    if ((fd_socket_just_connected = accept(server_sock, (struct sockaddr *)&client, &size_sockaddr)) == -1){
                        syserr("server : accept failed");
                        return EXIT_FAILURE;
                    }
                    FD_SET(fd_socket_just_connected, &active_fd_set);
                    fd_clients[fd_socket_just_connected] = fdopen(fd_socket_just_connected, "w+");
                    char startQCM='o';
                    bool notAutorized =cantAnswerToQcm(&startQCM,fd_socket_just_connected);
                    
                    if ((nb_fread = fwrite(&startQCM, sizeof(startQCM), 1, fd_clients[fd_socket_just_connected])) != 1){
                        puts("server : fwrite failed");
                    }
                    fflush(fd_clients[fd_socket_just_connected]);

                    printf("\nConnection from %s : %d accepted for client with id : %d\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port), fd_socket_just_connected);
                    NB_MAX_CLIENT_UTILS++;
                    
                    if(notAutorized){
                        if(fclose(fd_clients[fd_socket_just_connected]) !=0){
                            syserr("server : ERROR on closing \n");
                        }
                        printf("server : Client %d disconnected because he is not autorized\n", fd_socket_just_connected);
                        FD_CLR(fd_socket_just_connected, &active_fd_set);
                        NB_MAX_CLIENT_UTILS--;
                    }               
                }else{
                    if ((nb_fread = fread(buffer, sizeof(struct clientStructure), 1, fd_clients[idClient])) != 1){
                        syserr("server : fread failed");
                        if(fclose(fd_clients[idClient]) !=0){
                            syserr("server : ERROR on closing client \n");
                        }
                        printf("server : Client %d disconnected\n", idClient);
                        FD_CLR(idClient, &active_fd_set);
                        break;
                    }

                    memcpy(&clientStructureSent,buffer,sizeof(clientStructureSent));
                    printClientResponses(clientStructureSent.responseBuffer, idClient);

                    if (endAskQuestionsForClient(&clientStructureSent,clientEndScores,lengthStructClientEndScores)){      
                        calculateScoreClient(&lengthStructClientEndScores,clientEndScores, &clientStructureSent, idClient);
                        if(choseStateClient(&clientStructureSent,clientEndScores,lengthStructClientEndScores)){
                            showScoreForServer(clientStructureSent);
                        }
                    }

                    memcpy(buffer,&clientStructureSent,sizeof(buffer));
                    if ((nb_fwrite = fwrite(buffer, sizeof(struct clientStructure), 1, fd_clients[idClient])) != 1){
                        puts("server : fwrite failed");
                    }
                    fflush(fd_clients[idClient]);

                    if (endOfGame(clientEndScores, &active_fd_set, &NB_MAX_CLIENT_UTILS, &lengthStructClientEndScores, fd_clients, clientStructureSent))
                        break;
                    
                }
            }
        }
    }
    return EXIT_SUCCESS;
}