#ifndef CONSTANTES_H
#define CONSTANTES_H

#define LENGTH_QCM 5
#define NUMBER_QUESTIONS_JAVA  5
#define NUMBER_QUESTIONS_C  5
#define NUMBER_TYPES_QCM  2
#define NUMBER_CLIENT 6
#define true 1
#define false 0
#define LENGTH_PACKET 1024
#define PORT  8080
#define IP_ADRESS "127.0.0.1"
#define BUFFER_RESPONSES_LENGTH 4


#endif