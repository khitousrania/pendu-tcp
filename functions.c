#include<stdio.h>
#include<stdbool.h> 
#include<sys/socket.h>
#include<arpa/inet.h>	
#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include "functions.h"
#include "constantes.h"
#include "qcm.h"
#include "errhandlers.h"
#include "structures.h"

bool connectionServer(int * server_sock,struct sockaddr_in * server,fd_set * active_fd_set){
    if((*server_sock = socket(AF_INET , SOCK_STREAM , 0))<0){
        syserr("server : Could not create socket");
        return EXIT_FAILURE;
    }
	puts("server : Socket created");
    (*server).sin_family = AF_INET;
	(*server).sin_addr.s_addr = inet_addr(IP_ADRESS); 
	(*server).sin_port = htons(PORT);
    if( bind((*server_sock),(struct sockaddr *)server , sizeof(*server)) < 0){
		syserr("server : bind failed");
		return EXIT_FAILURE;
	}
	puts("server : bind done");
    if(listen((*server_sock) , NUMBER_CLIENT)<0){
        syserr("server : listen failed");
		return EXIT_FAILURE;
    }
	puts("server : listen done");
    FD_ZERO (active_fd_set);
    FD_SET ((*server_sock), active_fd_set);
    puts("\n************************DASHBORD************************");
    puts("Waiting for incoming connections...\n");
    return EXIT_SUCCESS;

}


bool connectionClient(int * fd_sock, struct sockaddr_in * server_sock){
   if((*fd_sock = socket(AF_INET , SOCK_STREAM , 0))<0){
        syserr("client : Could not create socket");
        return EXIT_FAILURE;
    }
	puts("client : Socket created");
	(*server_sock).sin_family = AF_INET;
	(*server_sock).sin_addr.s_addr = inet_addr(IP_ADRESS); 
	(*server_sock).sin_port = htons(PORT);
	
	if (connect((*fd_sock) , (struct sockaddr *)server_sock , sizeof(*server_sock)) < 0){
		syserr("client : connection to server failed");
		return EXIT_FAILURE;
	}
    puts("client : Client connected\n");  
    return EXIT_SUCCESS;
}

bool welcomeClient(char connected){
    if(connected =='o'){
        puts("WELCOME TO THE HANGED GAME \n");
        puts("  _______     ");
        puts(" |       |    ");
        puts(" |      / \\  ");
        puts(" |      \\_/  ");
        puts(" |      _|_   ");
        puts(" |     /| |\\ ");
        puts(" |    / |_| \\");
        puts(" |       ^    ");
        puts(" |      / \\  ");
        puts(" |     /   \\ ");
        puts("_|____________\n");
        return true;
    }else{
        puts("2 Clients are connected, pleaze wait before trying again !");
        return false;
    }
}

bool cantAnswerToQcm(char *startQcm, int fd_socket_just_connected){
    if (fd_socket_just_connected > 5){
        (*startQcm) = 'n';
        return true;
    }
    return false;
}
void initializeBuffer(char * bufferResponses){
    for(int i =0;i< BUFFER_RESPONSES_LENGTH; i++){
        bufferResponses[i]='0';
    }

}
void printClientResponses(char * bufferResponses, int idClient){
    printf("Responses of the client : %d\t ",idClient);
    for(int k =0; k< BUFFER_RESPONSES_LENGTH;k++){
        printf("%d : %c |", k, bufferResponses[k] != '\n' ? bufferResponses[k] : '0');
    }
    printf("\n");
}

void fixNumberOfQuestions(struct clientStructure * clientStructureSent){
    if((*clientStructureSent).numberNextQuestion ==0){
        if((*clientStructureSent).idLangageChosen =='1' ){
            (*clientStructureSent).numberOfQuestions =NUMBER_QUESTIONS_JAVA;
        }else{
            (*clientStructureSent).numberOfQuestions =NUMBER_QUESTIONS_C;  
        }
    }
}

void askingClientToChoseLangage(struct clientStructure * clientStructureSent,char * message){
    (*clientStructureSent).numberNextQuestion = 0;
	strcpy((*clientStructureSent).qcm , "");
	puts(questionChoseQcm);
	do{
		fgets(message, sizeof(message), stdin);
		if(message[0] != '1' && message[0] != '2'  )
			puts("You must choose 1 for JAVA or 2 for C !");
	}while(message[0] != '1' && message[0] != '2'  );
	
	(*clientStructureSent).idLangageChosen = message[0];
	initializeBuffer((*clientStructureSent).responseBuffer);
    (*clientStructureSent).finalScore =0;
    
}

bool endAskQuestionsForClient(struct clientStructure * clientStructureSent,struct clientEndScores * clientEndScores,int lengthStructClientEndScores){    
    (*clientStructureSent).numberOfOtherClient = (int8_t)lengthStructClientEndScores;
    fixNumberOfQuestions(clientStructureSent);
    if ((*clientStructureSent).numberNextQuestion == (*clientStructureSent).numberOfQuestions || (
    (*clientStructureSent).numberNextQuestion == (*clientStructureSent).numberOfQuestions-1 && (*clientStructureSent).numberOfOtherClient ==0  )){
        return true;
    }else if((*clientStructureSent).idLangageChosen =='2' ){
        if(clientEndScores[lengthStructClientEndScores-1].idLangageChosen =='2' && (*clientStructureSent).numberNextQuestion ==4 )
            strcpy((*clientStructureSent).qcm , compareSameLangage);
        else
            strcpy((*clientStructureSent).qcm , cQcm[(*clientStructureSent).numberNextQuestion]);
    }else{
        if(clientEndScores[lengthStructClientEndScores-1].idLangageChosen =='1' && (*clientStructureSent).numberNextQuestion ==4)
            strcpy((*clientStructureSent).qcm , compareSameLangage);
        else
            strcpy((*clientStructureSent).qcm , javaQcm[(*clientStructureSent).numberNextQuestion]);

    }
    (*clientStructureSent).numberNextQuestion++;
    return false;
}

bool choseStateClient( struct clientStructure *  clientStructure,struct clientEndScores clientEndScores[],int  lengthStructClientEndScores){
    if (clientStructure->finalScore == BUFFER_RESPONSES_LENGTH){
        clientStructure->state = 'w';
    }else if(clientStructure->finalScore < BUFFER_RESPONSES_LENGTH/2){ 
        clientStructure->state='l';
    }else if((clientStructure->finalScore ==2 || clientStructure->finalScore ==3) && lengthStructClientEndScores >1 && (clientStructure->responseBuffer[4]=='1')){   
            if(clientEndScores[1].endScore > clientEndScores[0].endScore)
                clientStructure->state = 'w';
            else if(clientEndScores[1].endScore < clientEndScores[0].endScore)
                clientStructure->state='l';
            else 
                clientStructure->state='e';
    }else{
        if( clientStructure->finalScore == (BUFFER_RESPONSES_LENGTH/2)  || clientStructure->finalScore  > (BUFFER_RESPONSES_LENGTH/2))
            clientStructure->state = 'w';       
    }
    if((clientStructure->state)== ' ')
        return false;
    clientEndScores[lengthStructClientEndScores].state = clientStructure->state;
    return true;

    
}
void calculateScoreClient( int8_t * lengthStructClientEndScores,struct clientEndScores * clientEndScores, struct clientStructure *  clientStructure,int idClient){  
    (*clientStructure).idClient = (int8_t)idClient;
    (*clientStructure).finalScore = 0;
    (*clientStructure).state = ' ';
    printf("START CALCUL SCORE FOR CLIENT %d\n", (*clientStructure).idClient);    
    if((*clientStructure).finalScore==0){
        for(int i=0 ; i<BUFFER_RESPONSES_LENGTH;i++){
            if((*clientStructure).idLangageChosen =='1'){
                if(indexRightResponsesJava[i] ==(*clientStructure).responseBuffer[i])
                    ++(*clientStructure).finalScore;
            }else if((*clientStructure).idLangageChosen =='2'){
                if(indexRightResponsesC[i] ==(*clientStructure).responseBuffer[i])
                    ++(*clientStructure).finalScore;            
            }
        }
        (clientEndScores[*lengthStructClientEndScores]).idClient = (int)(*clientStructure).idClient;
        (clientEndScores[*lengthStructClientEndScores]).endScore=(int)(*clientStructure).finalScore;
        (clientEndScores[*lengthStructClientEndScores]).idLangageChosen=(*clientStructure).idLangageChosen;
        (*lengthStructClientEndScores)++;
    
        printf("Score of the client  %d is %d/%d\n" ,clientStructure->idClient,clientStructure->finalScore,BUFFER_RESPONSES_LENGTH);    
    }
}
void showScoreForServer(struct clientStructure  clientStructureSent){
        if(clientStructureSent.state =='e')
            printf("Clients %d have the same score with the first client\n",clientStructureSent.idClient);  
        else if(clientStructureSent.state =='w')
            printf("Client %d won the game\n", clientStructureSent.idClient);
        else if(clientStructureSent.state =='l')
            printf("Client %d lose the game\n", clientStructureSent.idClient);
    
}

void showScoresForClient(struct clientStructure clientStructure){
    puts("\n***************** RESULT *********************\n");
    
    if(clientStructure.finalScore ==BUFFER_RESPONSES_LENGTH){
        printf("Congratulations shampion ! your score is %d / %d\n", clientStructure.finalScore,BUFFER_RESPONSES_LENGTH);    
	    puts("SO YOU ARE FREE :) ");
        puts("    _    ");
	    puts("   / \\  ");
	    puts("   \\_/  ");
	    puts("    _|_   ");
	    puts("   /| |\\ ");
	    puts("  / |_| \\");
	    puts("     ^    ");
	    puts("    / \\  ");
	    puts("   /   \\ ");
	    puts("__________\n");        
    }else if(clientStructure.finalScore <BUFFER_RESPONSES_LENGTH/2){
        printf("OH OH ! your score is %d / %d \n", clientStructure.finalScore,BUFFER_RESPONSES_LENGTH);    
	    puts("SO YOU ARE NOT FREE :'( ");
        puts("  _______     ");
	    puts(" |       |    ");
	    puts(" |      / \\  ");
	    puts(" |      \\_/  ");
	    puts(" |      _|_   ");
	    puts(" |     /| |\\ ");
	    puts(" |    / |_| \\");
	    puts(" |       ^    ");
	    puts(" |      / \\  ");
	    puts(" |     /   \\ ");
	    puts("_|____________\n");        
    }else if((clientStructure.finalScore ==2 || clientStructure.finalScore==3) && clientStructure.state =='w' && clientStructure.numberOfOtherClient !=0 &&
    clientStructure.responseBuffer[4] =='1' ){
        printf("Congratulations you won against the other gamer  ! your score is %d / %d \n", clientStructure.finalScore,BUFFER_RESPONSES_LENGTH);    
	    puts("SO YOU ARE FREE :) ");
        puts("    _    ");
	    puts("   / \\  ");
	    puts("   \\_/  ");
	    puts("    _|_   ");
	    puts("   /| |\\ ");
	    puts("  / |_| \\");
	    puts("     ^    ");
	    puts("    / \\  ");
	    puts("   /   \\ ");
	    puts("__________\n");  
    }else if((clientStructure.finalScore ==2 || clientStructure.finalScore==3) && clientStructure.state =='l' && clientStructure.numberOfOtherClient !=0 &&
    clientStructure.responseBuffer[4] =='1' ){
        printf("OH OH ! you lose against the other gamer and your score is %d / %d \n", clientStructure.finalScore,BUFFER_RESPONSES_LENGTH);     
	    puts("SO YOU ARE NOT FREE :'( ");
        puts("  _______     ");
	    puts(" |       |    ");
	    puts(" |      / \\  ");
	    puts(" |      \\_/  ");
	    puts(" |      _|_   ");
	    puts(" |     /| |\\ ");
	    puts(" |    / |_| \\");
	    puts(" |       ^    ");
	    puts(" |      / \\  ");
	    puts(" |     /   \\ ");
	    puts("_|____________\n"); 
    }else if((clientStructure.finalScore ==2 || clientStructure.finalScore==3) && clientStructure.state =='e' && clientStructure.numberOfOtherClient !=0 &&
    clientStructure.responseBuffer[4] =='1' ){
        printf("Congratulations ! Same result with the other gamer and your score is %d/%d \n", clientStructure.finalScore,BUFFER_RESPONSES_LENGTH);     
	    puts("SO YOU ARE FREE :) ");
        puts("    _    ");
	    puts("   / \\  ");
	    puts("   \\_/  ");
	    puts("    _|_   ");
	    puts("   /| |\\ ");
	    puts("  / |_| \\");
	    puts("     ^    ");
	    puts("    / \\  ");
	    puts("   /   \\ ");
	    puts("__________\n");  
    }else{
        printf("Congratulations ! your score is %d/%d \n", clientStructure.finalScore,BUFFER_RESPONSES_LENGTH);
	    puts("SO YOU ARE FREE :) ");
        puts("    _    ");
	    puts("   / \\  ");
	    puts("   \\_/  ");
	    puts("    _|_   ");
	    puts("   /| |\\ ");
	    puts("  / |_| \\");
	    puts("     ^    ");
	    puts("    / \\  ");
	    puts("   /   \\ ");
	    puts("__________\n");  
    }

}

bool endOfGame(struct clientEndScores *clientEndScores,fd_set * active_fd_set,int * NB_MAX_CLIENT_UTILS,int8_t * lengthStructClientEndScores,FILE ** fd_clients,struct clientStructure clientStructureSent){
    if (clientStructureSent.state == 'w' || clientStructureSent.state == 'l' || clientStructureSent.state == 'e'){
         if(fclose(fd_clients[(int)(clientStructureSent.idClient)]) !=0){
            syserr("server : ERROR on closing client\n");
        }
        FD_CLR((int)clientStructureSent.idClient, active_fd_set);
        printf("server : client %d disconnected\n", clientStructureSent.idClient);
        if(*lengthStructClientEndScores==2){
            puts("server : MAX CLIENTS STARTING DELETING HISTORY");
            *NB_MAX_CLIENT_UTILS = 0;
            for (int length = 0; length < 2; length++)
                memset(&(clientEndScores[length]), '\0', sizeof(&clientEndScores));
            *lengthStructClientEndScores=0;
            puts("server : HISTORY DELETED READY TO RECEIVE 2 NEW CLIENTS\n");
        }
        return true;
    }
    return false;
}


 