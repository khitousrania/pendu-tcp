#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include<stdbool.h>  
#include<stdio.h>
#include "structures.h"

bool connectionServer(int * server_sock,struct sockaddr_in * server,fd_set * active_fd_set);
bool connectionClient(int * fd_sock, struct sockaddr_in * socket);
bool welcomeClient(char connected);
bool cantAnswerToQcm(char *startQcm, int fd_socket_just_connected);
void printClientResponses(char * bufferResponses, int idClient);
void initializeBuffer(char * bufferResponses);
void fixNumberOfQuestions(struct clientStructure * clientStructureSent);
bool endAskQuestionsForClient(struct clientStructure * clientStructureSent,struct clientEndScores * clientEndScores,int lengthStructClientEndScores);
void askingClientToChoseLangage(struct clientStructure * clientStructureSent,char * message);
bool choseStateClient(struct clientStructure *clientStructure, struct clientEndScores clientEndScores[], int lengthStructClientEndScores);
void calculateScoreClient(int8_t *lengthStructClientEndScores, struct clientEndScores *clientEndScores, struct clientStructure *clientsStructure, int idClient);
void showScoreForServer(struct clientStructure clientStructureSent);
void showScoresForClient(struct clientStructure clientStructure);
bool endOfGame(struct clientEndScores *clientEndScores,fd_set * active_fd_set,int * NB_MAX_CLIENT_UTILS,int8_t * lengthStructClientEndScores,FILE ** fd_clients,struct clientStructure clientStructureSent);


#endif 