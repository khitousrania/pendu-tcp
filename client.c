#include <stdio.h>	
#include <stdlib.h>	
#include <string.h>	
#include <sys/socket.h>	
#include <arpa/inet.h>	
#include "constantes.h"
#include "functions.h" 
#include "structures.h"
#include "errhandlers.h" 
 
int main(){

	int fd_socket_client;
	struct sockaddr_in server;
	if(connectionClient(&fd_socket_client,&server))
		return EXIT_FAILURE;
	
    FILE* fd_server = fdopen(fd_socket_client, "w+");
    if ( fd_server == NULL ){
        syserr("client : fdopen failed"); 
        return EXIT_FAILURE;
    }
	
	size_t nb_read ;
	char connected;
	if((nb_read=fread(&connected , sizeof(connected),1,fd_server))!= 1){
		syserr("client : fread 1 failed");
		if(fclose(fd_server) != 0){
			syserr("client : ERROR on closing\n");
		}
		return EXIT_FAILURE;
	}

	
	if(!welcomeClient(connected))
		return EXIT_FAILURE;
	
	char message[12] ;
	struct clientStructure clientStructureSent;
	askingClientToChoseLangage(&clientStructureSent, message);

	size_t nb_write ;
	char buffer[BUFSIZ];
    memset(buffer, '\0', BUFSIZ);

	memcpy(buffer,&clientStructureSent,sizeof(buffer));
	if((nb_write=fwrite(buffer, sizeof(struct clientStructure),1,fd_server)) != 1){
		syserr("client : fwrite 1 failed");
		if(fclose(fd_server) != 0){
			syserr("client : ERROR on closing\n");
		}
		return EXIT_FAILURE;
	}
	fflush(fd_server);

	int idResponse =0;
	int counter =0;
	int stopNow = 0;
	do{
		if((nb_read=fread(buffer , sizeof(struct clientStructure),1,fd_server))!= 1){
			syserr("client : fread 2 failed");
			if(fclose(fd_server) != 0){
				syserr("client : ERROR on closing\n");
			}
			return EXIT_FAILURE;
		}else{
			memcpy(&clientStructureSent,buffer,sizeof(clientStructureSent));
			if(counter== (LENGTH_QCM-1) && clientStructureSent.numberOfOtherClient == 0){
				strcpy( clientStructureSent.responseBuffer ,"0\n");
				stopNow = 1;
			}else if(counter==LENGTH_QCM){
				break;
			}else{
		 	    puts(clientStructureSent.qcm);	
				printf("Pleaze, chose one numeric degit corresponding to your answer\n");
				printf("Response :");
				fgets(message, sizeof(message), stdin);	
				clientStructureSent.responseBuffer[idResponse] =message[0];
			}if(stopNow==1){
				break;
			}
			memcpy(buffer,&clientStructureSent,sizeof(buffer));
			if((nb_write=fwrite(buffer, sizeof(struct clientStructure),1,fd_server)) != 1){
				syserr("client : fwrite 2 failed");
				if(fclose(fd_server) != 0){
					syserr("client : ERROR on closing\n");
				}
				return EXIT_FAILURE;
			}
			fflush(fd_server);
			idResponse++;
		}
		counter++;
		
	}while(counter<=LENGTH_QCM);
	
	showScoresForClient(clientStructureSent);
	
	return EXIT_SUCCESS;
}