#ifndef QCM_H
#define QCM_H

char * questionChoseQcm ="Please choose one of the following languages\n 1-JAVA \n 2-C ";
char * javaQcm[5] ={
    "Does the java.nio package exist in Java?\n 1-Yes, this package exists. \n 2-No this package does not exist.\n",
    "Which Java concept is a way to convert real-world objects into class terms?\n 1-Polymorphisme \n 2-Encapsulation \n 3-Abstraction \n 4-Legacy \n ",
    "Which keywords that are not used to specify the visibility of properties and methods?\n 1-final \n 2-private\n 3-abstract \n 4-protected \n 5-public \n",
    "The Factory design pattern is a design model for the implementation of customer communication server\n 1-Yes \n 2-No\n",
    "Another client has already submitted their answers before you but not with same type, do you want to compare your score with them? \n 1-Yes \n 2-No \n"
};
char   indexRightResponsesJava[5] ={'2','4','4','2','0'};


char * cQcm[5] ={
    "Is it possible to define C functions with a variable number of parameters? \n 1-Yes \n 2-Non \n",
    "What is the purpose of the va_arg function? \n 1-To value an argument passed as a parameter to main. \n 2-To retrieve the value of a parameter on a function with a variable number of parameters.\n",
    "What is the null keyword in C? \n 1-It represents the null address (0). \n 2-It allows a variable to be reset to the neutral value of the type in question. \n 3-null n'est pas un mot clé du langage.\n ",
    "What is a pointer? \n 1-It is simply an address in memory. \n 2-A concept introduced by the <stdlib.h> library. \n 3-A concept introduced by the <string.h> library.\n ",
    "Another client has already submitted their answers before you but not with same type, do you want to compare your score with them? \n 1-Yes \n 2-No \n"
};

char indexRightResponsesC[5] ={'1','2','1','1','0'};

char * compareSameLangage = "Another client has already submitted their answers before you with the same type, do you want to compare your score with them ? \n 1-Yes \n 2-No \n ";

#endif