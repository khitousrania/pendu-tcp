EXEC=server
EXEC_2=client
SOURCES=server.c stdmsghandlers.c  functions.c
SOURCES_2 = client.c stdmsghandlers.c  functions.c
OBJECTS=$(SOURCES:.c=.o)
OBJECTS_2=$(SOURCES_2:.c=.o)
CC=gcc
CFLAGS=-W -Wall -Wconversion
 
.PHONY: default clean
default: $(EXEC)
default: $(EXEC_2)
 
 
server.o: server.c  constantes.h functions.h structures.h qcm.h
client.o: client.c constantes.h functions.h structures.h qcm.h

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)
 
$(EXEC): $(OBJECTS)
	$(CC) -o $@ $^

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

$(EXEC_2): $(OBJECTS_2)
	$(CC) -o $@ $^
 
clean:
	rm -rf $(EXEC) $(EXEC_2) $(OBJECTS) $(OBJECTS_2) $(SOURCES:.c=.c~) $(SOURCES:.c=.h~) $(SOURCES_2:.c=.c~) $(SOURCES_2:.c=.h~) Makefile~